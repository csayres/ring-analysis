#!/usr/bin/env python
"""
Fit circles to images, after thresholding and identifying regions.

Circle fitting method stolen from method 2 (leastsq) here:
    http://wiki.scipy.org/Cookbook/Least_Squares_Circle

Original version by: John Parejko, 2015-03-02
"""

import os.path

import numpy as np
import scipy.ndimage as nd
import scipy.optimize
import matplotlib.pyplot as plt
from matplotlib.patches import Circle

def greyscale(image):
    """Remove PNG alpha and sum colors to make greyscale."""
    # remove the alpha channel from a PNG
    if image.shape[2] == 4:
        image = image[:,:,0:3]
    image = image.sum(axis=2) # sum color channels.
    return image

def threshold(image, value=None):
    """Zero-clip an image to value."""
    if value is None:
        value = 200
    result = image.copy()
    clip = image < value
    result[clip] = 0
    return result

def label(image):
    """Label contiguous regions, and find the two largest ones."""
    labels,n = nd.measurements.label(image)
    # The regions we want will be the largest, ignoring the 0 region.
    size = np.array([(labels == i).sum() for i in range(1,n)])
    large_regions = np.arange(n)[size > 500]
    return labels, [labels == i+1 for i in large_regions]


def implicit_circle(beta,x):
    """For ODR method."""
    return (x[0]-beta[0])**2 + (x[1]-beta[1])**2 - beta[2]**2

def calc_r(xc, yc, data=None):
    """For leastsq method."""
    return np.sqrt((data[0]-xc)**2 + (data[1]-yc)**2)

def distance(center, data):
    """Algebraic distance between data and a circle centered at center."""
    Ri = calc_r(*center,data=data)
    return Ri - Ri.mean()

def explicit_circle(data,x0,y0,r):
    """for curve_fit"""
    return r * np.sqrt((data[0]-x0)**2 + (data[1]-y0)**2)

def fit_circle(image, mask):
    """Fit a circle to the image, with a mask."""
    xx,yy = np.meshgrid(range(image.shape[0]),range(image.shape[1]))
    xx = xx[mask]
    yy = yy[mask]
    center_estimate = xx.mean(),yy.mean()
    center, ier = scipy.optimize.leastsq(distance, center_estimate, args=[xx,yy])
    ri = calc_r(*center,data=[xx,yy])
    r = ri.mean()
    residuals = sum((ri - r)**2)
    # print r,center,residuals
    return {'r':r,'center':center,'resid':residuals}
    # use the image as the weights (1/sigma in curve_fit)
    # result, pcov = scipy.optimize.curve_fit(explicit_circle, [xx,yy], )

def draw_circle_on_each_axis(axes,fit):
    for axis1 in axes:
        for axis in axis1:
            circle = Circle(fit['center'],fit['r'],color='black',linewidth=2,fc='none',transform=axis.transData)
            axis.add_patch(circle)

def plot_result(name, image, clipped, labels, large, fits):
    """Make a 4x plot of the various steps, with the fitted circle over each."""
    fig, ax = plt.subplots(2,2,sharex=True,sharey=True,figsize=(8,8))
    fig.subplots_adjust(hspace=0,wspace=0)
    ax[0,0].imshow(image,aspect='equal',origin='lower')
    ax[0,1].imshow(clipped,aspect='equal',origin='lower')
    ax[1,0].imshow(labels,aspect='equal',origin='lower')
    xx,yy = np.meshgrid(range(image.shape[0]),range(image.shape[1]))
    for region in large:
        ax[1,1].plot(xx[region],yy[region],'.')
    for fit in fits:
        draw_circle_on_each_axis(ax,fit)

    plt.xlim(0,image.shape[0])
    plt.ylim(0,image.shape[1])

    plt.savefig('output/%s.png'%name)

def analyze_image(name, image, thresh=None, plot=True):
    """Threshold, label, fit."""
    image = greyscale(image)
    clipped = threshold(image,thresh)
    labels,regions = label(clipped)
    # r_guesses = [28,50]
    fits = []
    for region in regions:
        mask = region != 0
        fits.append(fit_circle(image,mask))

    if plot:
        plot_result(name, image, clipped, labels, regions, fits)

    return fits

def analyze_and_collate(files, threshold, plot=True):
    """Analyze the images, and collate the results."""
    outname = os.path.split(files[0])[0] + '.csv'

    names = ['N','r0','r1','x0','y0','x1','y1','delta_r','delta_x','delta_y']
    types = [np.int,] + [np.float64,]*(len(names)-1)
    format = ['%d',] + ['%10.8f']*(len(names)-1)
    dtype = zip(names,types)
    result = np.empty(len(files),dtype=dtype)

    for i,file in enumerate(files):
        image = nd.imread(file)
        name = os.path.splitext(os.path.split(file)[1])[0]
        fits = analyze_image(name, image, threshold, plot=plot)
        N = name[1:] # my image names are PXXXXXXX, so strip off the 'P'
        d_c = fits[0]['center']-fits[1]['center']
        result[i] = (N, fits[0]['r'],fits[1]['r'],
                     fits[0]['center'][0],fits[0]['center'][1],
                     fits[1]['center'][0],fits[1]['center'][1],
                     fits[0]['r']-fits[1]['r'],
                     d_c[0],d_c[1])

    np.savetxt(outname, result, header=' '.join(names), fmt=format)

def main():
    import argparse
    desc = 'Find one or more circles in square image[s] that are roughly centered on the circle[s].'
    usage=' %(prog)s [OPTIONS] FILES'
    parser = argparse.ArgumentParser(description=desc,usage=usage)
    parser.add_argument("files", help="Files to process", type=str, nargs='+')
    parser.add_argument("--threshold", '-t', help="Threshold to cut the color-stacked images at", type=int)
    parser.add_argument("--plot", '-p', help="Generate a 4-up plot of the fit results", action='store_true')
    args = parser.parse_args()

    analyze_and_collate(args.files,args.threshold,plot=args.plot)

if __name__ == '__main__':
    main()
