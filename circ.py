#!/usr/bin/env python
"""The basic strategy if this routine is to convert a x,y image
to an r,theta image, after finding the exact ring center pixel.
If you find the exact center and the ring is perfectly circular
you will get a flat line in r,theta.

But this doesn't really happen so what I recommend is searching a
region around the (human picked) center of the circle, computing
the r,theta conversion using that origin.  Collapsing along theta
(we assume circularity).  This gives you a radial distribution.
Pick the center that gives you the tightest radial distrubution.  The
peak of the distrubtion is your radius in pixels.
"""
import numpy
import numpy.linalg
import scipy.ndimage as nd
import matplotlib.pyplot as plt

def extract2DRegion(img, ctr, boxSize):
    """ img: image
        ctr: [x,y]
        boxSize: pixels
    """
    # note y is axis=0, x is axis =1
    return img[(ctr[1]-boxSize/2):(ctr[1]+boxSize/2),(ctr[0]-boxSize/2):(ctr[0]+boxSize/2)]

def polarCnv(img):
    # plot in polar coords
    # y = radius
    # x = theta
    r = []
    theta = []
    counts = []
    ctr = numpy.asarray(img.shape)/2
    boxLen = img.shape[0]
    for yCoord in range(boxLen):
        for xCoord in range(boxLen):
            r.append(numpy.linalg.norm([yCoord-ctr[0], xCoord-ctr[0]]))
            theta.append(numpy.arctan2(yCoord-ctr[0], xCoord-ctr[0])/numpy.pi)
            counts.append(img[xCoord, yCoord])
    return numpy.asarray(r), numpy.asarray(theta), numpy.asarray(counts)


fPath = './img/ring one.tif'
img = nd.imread(fPath)
roughCenter = numpy.array([915, 370])
roughBoxSize = (1170-910)*2.8
img = numpy.sum(img, axis=2)
newImg = extract2DRegion(img, roughCenter, roughBoxSize)
plt.figure()
plt.imshow(newImg)
#plt.plot(roughCenter[0],roughCenter[1], 'or', ms=110)
r, theta, counts = polarCnv(newImg)
plt.figure()
plt.hexbin(theta, r, counts)
plt.xlabel('theta/pi')
plt.ylabel('radius')
plt.figure()
plt.plot(r, counts, '.k')
plt.xlabel('radius')
plt.ylabel('counts')

optRad = 218
spread = 100
bgAnn = [60, 120] #pixels

# bgSliceLow = numpy.logical_and(r<optRad - bgAnn[0], r>optRad - bgAnn[1])
# bgSliceHigh = numpy.logical_and(r>optRad + bgAnn[0], r<optRad + bgAnn[1])
# bgLog = numpy.logical_and(bgSliceHigh, bgSliceLow)
# # bgLowLevel = counts[optRad-bgAnn[1]:optRad-bgAnn[0]]
# # print bgLowLevel
# # bgHighLevel = counts[optRad+bgAnn[0]:optRad+bgAnn[1]]
# bgLevel = numpy.median(counts[bgLog])
# print 'bglevel', bgLevel

logSlice = numpy.logical_and(r > optRad-spread,  r < optRad + spread)
rZoom = r[logSlice]
cZoom = counts[logSlice]
plt.figure()
plt.plot(rZoom,cZoom-numpy.median(cZoom),'.k', ms=1)
plt.title('Background subtracted Counts')
plt.xlabel('radius')
plt.ylabel('counts')
print '0.5*spread / r', 0.5*20./220.
plt.show()


